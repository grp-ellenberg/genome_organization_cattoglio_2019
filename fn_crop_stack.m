function [outStack] = fn_crop_stack(inStack,cropDx, cropDy, nRemoveLowerSlices)
% This function crops inStack from image centre in xy and removes nRemoveLowerSlices 
% number slice from the lower part of the stack  

[inDx, inDy, inDz] = size(inStack);

outDz = inDz - nRemoveLowerSlices;

centX = round(inDx/2);
centY = round(inDy/2);

halfCropX = round(cropDx/2);
halfCropY = round(cropDy/2);

outStack = zeros(cropDx, cropDy, outDz);
for zplane = 1: outDz
    outStack(:,:,zplane) = inStack(centX-halfCropX:centX+halfCropX-1, centY-halfCropY:centY+halfCropY-1, zplane + nRemoveLowerSlices);    
end
end

