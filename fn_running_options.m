function [op, chr_op, cell_op] = fn_running_options()
%This function sets the default parameters and running options
%Author: Julius Hossain, EMBL, Heidelberg
%Last update: 2019-01-08

%% Parameters for main calling file
op.segCellBoundary = 1; % Set 0 to segment chromosome only
op.dispSegMass = 1;     % Set 1 to save 3d reconstructed chromosome.
op.nRemLowerSlices = 6; % number of lower slices to exclude from segmentation
op.chrSigInDxt = 0;     % Set 1 if chromosome signal is also prominent in dextral channel
op.cellChanIdx = 2;     % Channel index of dextran
op.chrChanIdx  = 3;     % Channel index of chromatin
op.poiChanIdx  = 1;     % Channel index of chromatin
op.cropFlag = 1;        % Crop images before segmentation
op.cropDx = 360;        % Number of rows in cropped image
op.cropDy = 360;        % Number of columns in cropped image

op.exParamsFn  = 'Extracted_parameters.txt'; % Name of the file to save the extracted parameters
op.combExParamsFn = 'Extracted_parameters_combined.txt';% Name of the file to save the extracted parameters from all the files
op.paramsName = {'File_path', 'N_cell', 'N_nuc', 'N_cyt', 'Con_cell_nM', 'Con_nuc_nM', 'Con_cyt_nM',...
    'Tot_int_cell', 'Tot_int_nuc', 'Tot_int_cyt', 'Vol_cell_mic3', 'Vol_nuc_mic3', 'Vol_cyt_mic3', ...
    'Vol_cell_pix', 'Vol_nuc_pix', 'Vol_cyt_pix', 'Cal_fact_nM', 'bg_int', 'Num_nuc'};
op.calFilename = 'calibration.txt'; %Name of the txt file containing calibration parameters
op.inFileExt = '*.lsm'; %Extension of raw image stacks
op.expDirRoot = 'Z:\Nike\Collaboration_AndersHansen\Data_ProcessedComputerMerle_NikeClicked\'; %Root directory containing the experimental data
op.outDirRoot = 'Z:\Nike\Collaboration_AndersHansen\Results_ProcessedComputerMerle_NikeClicked\';%Root directory to store the results

chr_op = fn_get_chr_seg_options();
cell_op = fn_get_cell_seg_options();
end

function [chr_op] = fn_get_chr_seg_options()
%% Parameters for chromosome segmentation
chr_op.sigmaBlur = 3;       %Sigma for Gaussian filter
chr_op.hSizeBlur = 5;       %Kernel size for Gaussian filter
chr_op.bFactor2D = 0.33;    %Contribution of 2D threshold
chr_op.splitVol = 2000;     %Minimum volume for to be considered for splitting
chr_op.mergeVol = 150;      %Maximum volume of a blob that is considered for merging
chr_op.minProphaseVol = 450;%Minimum volume to detect the first nucleus
chr_op.maxCentDisp = 20;    %Maximum distance from the centroid
chr_op.maxNoInitObj = 450;  %Maximum number of objects detected initially
chr_op.nObjRatio = 0.10;    %Portion to decide detected number of connected components are too high
end

function [cell_op] = fn_get_cell_seg_options()
%% Parameters for cell segmentation
%Structuring element for morphological operations
cell_op.se = strel(ones(3,3,3));
cell_op.rCRegion = 1;
cell_op.bFactorRatio = 0.20;

%Filtering parameters
cell_op.sigmaBlur = 5; %Sigma for negative staining channel
cell_op.hSizeBlur = 7; %Kernel size for negative staining channel

cell_op.sigmaRatio=3; %Sigma for gaussian blur in ratio image.
cell_op.hSizeRatio=5; %Kernel size for gaussian blur in ratio image

cell_op.sigmaChSeg=3; %Sigma for gaussian blur in chromosome segmentation
cell_op.hSizeChSeg=5; %Kernel size for gaussian blur in chromosome segmentation
cell_op.bFactorCh2D = 0.33; %Weight between 2d and 3d thresholds
cell_op.minVol = 500; %Min volume for the cell to be detected
cell_op.NA = 0.602214086; % Constant from Avogrado value

cell_op.color = {[0.05 0.5 0.15], [0.55 0.1 0.1]}; %Color for cell and chromosome surface rendering
cell_op.thresh = [0.5 0.5]; %Threshold for displaying the surface
cell_op.alpha = [0.45 0.7]; %Transperancies
cell_op.negVoxelsRatioInv = 0.001; %Portion of the neg stain voxels used for getting top intensity for invertion
cell_op. minCellVolume = 1000; %Minimum cell volume
cell_op.minObjArea = 10; %Minimum arear of a detected object in a slice
end
