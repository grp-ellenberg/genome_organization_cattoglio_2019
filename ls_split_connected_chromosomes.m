function [wsLabel] = ls_split_connected_chromosomes(chRegion, voxelSizeX, voxelSizeY, voxelSizeZ, bordImg, splitVol, mergeVol)
%This functions applies distance transform and watershed to separate
%chromosomes that are agglomerated together.

%It returns chromosome regions

% disRow = 5;
% disCol = 5;

%% Combining distance transform and watershed to identify cell region of interest
distImage = bwdistsc(~chRegion,[1 1 voxelSizeZ/voxelSizeX]);
tempVol = zeros(size(chRegion));
marker = zeros(size(chRegion));
voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ;

threeDLabelCh = bwconncomp(chRegion,18);   
for i=1: threeDLabelCh.NumObjects
    blobVol = length(threeDLabelCh.PixelIdxList{i}) * voxelSize;
    tempVol(:,:,:) = 0;
    tempVol(threeDLabelCh.PixelIdxList{i}) = 1;
    if blobVol < mergeVol && sum(sum(sum(tempVol .* bordImg))) ~= 0
        marker(threeDLabelCh.PixelIdxList{i}) = 1;
    elseif blobVol <=splitVol && blobVol >= mergeVol
        marker(threeDLabelCh.PixelIdxList{i}) = 1;
    elseif blobVol >splitVol
        tempVol = tempVol .*distImage;
        maxVal = max(max(max(tempVol(:,:,:))));
        tempVol(tempVol(:,:,:)<maxVal/2) = 0;
        tempVol(tempVol(:,:,:)>0) = 1;
        marker = or(marker, tempVol);
    end    
end
    
%displaySubplots(distImage, 'DT image', disRow, disCol, Nz, zFactor, 2);

%Display marker image
%displaySubplots(marker, 'CH marker', disRow, disCol, Nz, zFactor, 2);

distImage = -distImage; % invert distance image
%displaySubplots(distImage, 'DT of inverse', disRow, disCol, Nz, zFactor, 2);
distImage = imimposemin(distImage, marker, 18); %suppress local minima other than markers
%displaySubplots(distImage, 'DT after suppression', disRow, disCol, Nz, zFactor, 2);
distImage(~chRegion) = -Inf; % Set outside of cell region with infinity
%displaySubplots(distImage, 'DT after suppression - inf', disRow, disCol, Nz, zFactor, 2);
wsLabel = watershed(distImage); %Apply watershed algorithm

%Display watershed on distance image
%displaySubplots(wsLabel, 'Watershed on distance image', disRow, disCol, Nz, zFactor, 2);
%displaySubplots(wsLabel ==0, 'Watershed line', disRow, disCol, Nz, zFactor, 2);

wsLabel(~chRegion) = 0;
wsLabel(wsLabel(:,:,:)>0) = 1;
%displaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
clear marker;
clear tempVol;
clear distImage;
end