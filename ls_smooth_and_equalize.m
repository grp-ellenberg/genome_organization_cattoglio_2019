function [outStack] = ls_smooth_and_equalize(bwStack, refVol, voxelSize)
%This function smooth 3D bwStack and equalize to a given reference volume
smoothStack  = smooth3(bwStack(:,:,:) , 'box', 7);
outStack = double(smoothStack(:,:,:) >= 0.6);
outVol = sum(sum(sum(outStack))) *voxelSize;
thDist = 0.59;
while outVol <= refVol *0.995 && thDist >= 0.01
    outStack = double(smoothStack(:,:,:) >= thDist);
    outVol = sum(sum(sum(outStack))) *voxelSize;
    thDist = thDist -0.01;
end
end

